/**
 * Created by Gumis on 13.01.2016.
 */
public abstract class ParkingSpot implements Something1 , Something2{

    private int id;
    private static  int nextId;
    private boolean condition; // 1-busy 0-free
    private boolean preference ;
    private String who;

   public ParkingSpot(){
       this.id = nextId;
       nextId++;
       condition = false;
       who = "Empty";
    }

    public int getId(){return id;}
    public boolean getCondition(){return condition;}
    public String getWho(){return who;}

    public void setPreference(boolean preference){
        this.preference = preference;
    }

    public void entry(String nameSurname){
        condition = true;
        who = nameSurname;
    }

    public void departure(String nameSurname){
        condition = false;
        who = "Empty";
    }

    public String toString(){
        return "Id: " + id + " || " + "Condition: " + condition + " || " + "Preference: " + preference + " || " + "Who: " + who;
    }

    static{
        nextId = 1;
    }
}

