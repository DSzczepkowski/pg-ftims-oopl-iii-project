import java.io.*;
import java.util.*;

/**
 * Created by Gumis on 13.01.2016.
 */
public class CarPark {

    public static int menuCarPark(){
        System.out.println();
        System.out.println("     ****************************************");
        System.out.println("     *                 CarPark              *");
        System.out.println("     ****************************************");
        System.out.println("     1.Entry");
        System.out.println("     2.Where is my car ");
        System.out.println("     3.Administrator ");
        System.out.println("     0. End");

        Scanner input = new Scanner(System.in);
       // int w = input.nextInt();

        return input.nextInt();//w;
    }
    public static int menuEntry(){
        System.out.println();
        System.out.println("     ****************************************");
        System.out.println("     *                 Entry                *");
        System.out.println("     ****************************************");
        System.out.println("     1.Entry ");
        System.out.println("     2.Departure ");
        System.out.println("     0.Back ");

        Scanner input = new Scanner(System.in);
        //int w = input.nextInt();

        return input.nextInt();//w;//return w;
    }
    public static int menu(){
        System.out.println();
        System.out.println("     ****************************************");
        System.out.println("     *                              *");
        System.out.println("     ****************************************");
        System.out.println("     1.");
        System.out.println("     2. ");
        System.out.println("     3. ");
        System.out.println("     0. End");

        Scanner input = new Scanner(System.in);
       // int w = input.nextInt();

        return input.nextInt();//return w;
    }
    public static int menuAdministrator(){
        System.out.println();
        System.out.println("     ****************************************");
        System.out.println("     *                Administrator         *");
        System.out.println("     ****************************************");
        System.out.println("     1.Employees");
        System.out.println("     2.Condition ");
        System.out.println("     0.Back ");

        Scanner input = new Scanner(System.in);
       // int w = input.nextInt();

        return input.nextInt();//w;return w;
    }
    public static int menuEmployees(){
        System.out.println();
        System.out.println("     ****************************************");
        System.out.println("     *                 Employees             *");
        System.out.println("     ****************************************");
        System.out.println("     1.Add employee");
        System.out.println("     2.Remove employee ");
        System.out.println("     3.Display employees ");
        System.out.println("     0.Back ");

        Scanner input = new Scanner(System.in);
        //int w = input.nextInt();

        return input.nextInt();//w;return w;
    }
    public static String enterNameSurname(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the name ");
        String name = input.nextLine();
        System.out.println("Enter the surname ");
        String surname = input.nextLine();
        return name + " " + surname;
    }
    public static void serialized(Map employees){
        try
        {
            FileOutputStream fos = new FileOutputStream("hashmap.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(employees);
            oos.close();
            fos.close();
            System.out.printf("Serialized HashMap data is saved in hashmap.ser");
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }

    }

    public static void main(String[] args){
    //////////////////////////////////////////////////////////////////////////////////
        Map<String , Boolean> employees;
        Map<String , Integer> map= new HashMap<>();

        try
        {
            FileInputStream fis = new FileInputStream("hashmap.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            employees = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
             return;
        }catch(ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return ;
        }
        System.out.println("Deserialized HashMap..");
    /////////////////////////////////////////////////////////////////////////////////
        int n = 0;
        Set<Map.Entry<String, Boolean>> prefe = employees.entrySet();
        for (Map.Entry<String, Boolean> pref: prefe) {
            if( pref.getValue()){
                n++;
            }
        }
        //System.out.println(n);
       // prefe.clear();
    ///////////////////////////////////////////////////////////////////////////////////
        ParkingSpot[] parkingSpot = new ParkingSpot[20];
        for(int i = 0 ; i < n ; i++){
            parkingSpot[i] = new PrivilegedPlace();
        }
        for(int i = n ; i < 20; i++){
            parkingSpot[i] = new NormallyPlace();
        }
    //////////////////////////////////////////////////////////////////////////////////
        Scanner input = new Scanner(System.in);
        int menu = menuCarPark() ;
        while(menu!=0) {
            switch (menu) {
                case 1:
                    int menuEntry = menuEntry();

                    while (menuEntry != 0) {
                        switch (menuEntry) {
                            case 1:
                                String nameSurname =enterNameSurname();
                                if (employees.containsKey(nameSurname)) {
                                    if(employees.get(nameSurname)){
                                    for(int i = 0 ; i < n ; i++){
                                            if(!parkingSpot[i].getCondition()){
                                                parkingSpot[i].entry(nameSurname);
                                                parkingSpot[i].ent();
                                                map.put(nameSurname , i);
                                                break;
                                            }
                                        }
                                    }
                                    else{
                                        for(int i = n ; i < 20 ; i++){
                                            if(!parkingSpot[i].getCondition()){
                                                parkingSpot[i].entry(nameSurname);
                                                parkingSpot[i].ent();
                                                map.put(nameSurname , i);
                                                break;
                                            }
                                            else {
                                             if( i == 19){
                                                 System.out.println("No places");
                                                 break;
                                             }
                                                //break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    System.out.println("there is no such employee");
                                }
                                break;
                            case 2:
                                nameSurname =enterNameSurname();
                                if(map.containsKey(nameSurname)){
                                    parkingSpot[map.get(nameSurname)].departure(nameSurname);
                                    parkingSpot[map.get((nameSurname))].dep();
                                    map.remove(nameSurname);
                                }
                                else {
                                    System.out.println("there is no such employee");
                                }

                                break;
                            default:
                                System.out.println("wrong number");
                        }
                        menuEntry = menuEntry();
                    }
                    break;
                case 2:
                   /* int menus = menu();
                    while (menus != 0) {
                        switch (menus) {
                            case 1:

                                break;
                            default:
                                System.out.println("wrong number");
                        }
                        menus = menu();
                    }*/
                    String nameSurname = enterNameSurname();
                    if(map.containsKey(nameSurname)) {
                        //int id = map.get(nameSurname);
                        System.out.println("Your car is on site No." + (map.get(nameSurname) + 1));
                    }
                    else {
                        System.out.println("But perhaps today you came by bike");
                    }
                    break;

                case 3:
                    int menuAdministrator = menuAdministrator();
                    while (menuAdministrator != 0) {
                        switch (menuAdministrator) {
                            case 1:
                                int menuEmployee = menuEmployees();
                                while (menuEmployee != 0) {
                                    switch (menuEmployee) {
                                        case 1:
                                            nameSurname = enterNameSurname();
                                            if (employees.containsKey(nameSurname)) {
                                                System.out.println("Employee already exists");
                                            }
                                            else {
                                                System.out.println("Enter the preference(true/false) ");
                                                boolean preference = input.nextBoolean();
                                                employees.put(nameSurname, preference);
                                                System.out.println("Added employee ");
                                            }
                                            break;
                                        case 2:
                                            nameSurname = enterNameSurname();
                                            if (employees.containsKey(nameSurname)) {
                                                employees.remove(nameSurname);
                                                System.out.println("Removed employee ");
                                            }
                                            else {
                                                System.out.println("there is no such employee");
                                            }
                                            break;
                                        case 3:
                                            Set<Map.Entry<String, Boolean>> employee = employees.entrySet();
                                            for (Map.Entry<String, Boolean> employe: employee) {
                                                System.out.print("Full name: " + employe.getKey() + "  || " + " preference: " + employe.getValue());
                                                System.out.println();
                                            }
                                            break;
                                        default:
                                            System.out.println("wrong number");
                                    }
                                    menuEmployee = menuEmployees();
                                }
                                break;
                            case 2:
                                for(ParkingSpot x:parkingSpot){
                                    System.out.println(x);
                                }
                                break;
                            default:
                                System.out.println("wrong number");
                        }
                        menuAdministrator = menuAdministrator();
                    }
                    break;
                default:
                    System.out.println("wrong number");
            }
            menu = menuCarPark();
        }
        serialized(employees);
    }
}
