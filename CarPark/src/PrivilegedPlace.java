/**
 * Created by Gumis on 16.01.2016.
 */
public final class PrivilegedPlace extends ParkingSpot {

    public PrivilegedPlace(){
        super.setPreference(true);
    }

    //public final void entry(String nameSurname){
    //    super.entry(nameSurname);
    //    System.out.println("Entered the privileged place");
    //}
    public final void ent(){
        System.out.println( "Entered the privileged place");
    }

    //public final void departure(String nameSurname){
     //   super.departure(nameSurname);
     //   System.out.println("Released a privileged place");
    //}
    public final  void dep(){
        System.out.println( "Released a privileged place");
    }
}
